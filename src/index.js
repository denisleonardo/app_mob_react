import React from 'react';
import { Root } from "native-base";
import MainMenu from './MainMenu';
import Loading from './Loading';
import Signup from './Auth/Signup';
import Login from './Auth/Login';
import Variaveis from './Calculos/Variaveis';
import Velocidade from './Calculos/Velocidade';
import Vazao from './Calculos/Vazao';
import Calibracao from './Calculos/Calibracao';
import SelecaoPontas from './Calculos/SelecaoPontas';
import ListarPontas from './Calculos/ListarPontas';
//Navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

const Routes = createAppContainer(
    createStackNavigator({
        MainMenu: MainMenu,
        Loading: Loading,
        Login: Login,
        Signup: Signup,
        Variaveis: Variaveis,
        Velocidade: Velocidade,
        Vazao: Vazao,
        Calibracao: Calibracao,
        SelecaoPontas: SelecaoPontas,
        ListarPontas: ListarPontas,
    },
        {
            initialRouteName: 'Loading',
            /* The header config from HomeScreen is now here */
            defaultNavigationOptions: {
                header: null,
                headerMode: 'screen',
                headerStyle: {
                    backgroundColor: '#225f27',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }
        }
    )
);

export default () => <Root><Routes /></Root>;