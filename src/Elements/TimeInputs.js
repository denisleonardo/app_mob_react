import React, { Component } from 'react';
import { View, Text, Keyboard, TextInput } from 'react-native';


//Validar campo para hora
_validateHour = (text,func) => {
    if (/^([0-1]?[0-9]|2[0-3])?$/.test(text)) {
      func.changestate(text,func.name)
    }
}

//Validar campo para minutos
_validateMinute = (text,func) => {
    if (/^([0-5]?[0-9]|[0-9])?$/.test(text)) {
      func.changestate(text,func.name)
    }
}

//Validar campo para segundos
_validateSecond = (text,func) => {
    if (/^([0-5]?[0-9]|[0-9])?$/.test(text)) {
      func.changestate(text,func.name)
    }
}

export const HourInput = (props) => {
    return (
        <TextInput onChangeText={(e)=>this._validateHour(e,props)} {...props} keyboardType={'numeric'} />
    )
}


export const MinuteInput = (props) => {
    return (
        <TextInput onChangeText={(e)=>this._validateMinute(e,props)} {...props} keyboardType={'numeric'} />
    )
}

export const SecondInput = (props) => {
    return (
        <TextInput onChangeText={(e)=>this._validateMinute(e,props)} {...props} keyboardType={'numeric'} />
    )
}

