const Style = {
    contentCenter: {
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        flex: 1
    },
    contentFlex: {
        flex: 1,
        flexDirection: 'row',
        width: 275,
        justifyContent: 'space-between',
    },
    logoScreenLogin: {
        width: 160,
        height: 51,
        marginTop: 50,
        marginBottom: 30,
        alignSelf: 'center'
    },
    logoAllScreen: {
        width: 120,
        height: 38,
        marginTop: 10,
        marginBottom: 15,
        alignSelf: 'center'
    },
    formItem: {
        borderColor: 'transparent',
        alignItems: 'center',
        alignSelf: 'center',
        width: 275,
    },
    itemText: {
        borderColor: 'transparent',
        textAlign: 'center',
        width: 275,
        marginTop: 10
    },
    inputText: {
        marginTop: 5,
        width: 275,
        height: 40,
        fontSize: 14,
        borderColor: 'gray',
        borderWidth: 1,
        backgroundColor: '#FFFFFF',
    },
    inputTextResponsive: {
        marginTop: 5,
        height: 40,
        width: 128,
        fontSize: 14,
        borderColor: 'gray',
        borderWidth: 0,
        backgroundColor: '#FFFFFF',
    },
    inputTextResponsive3: {
        marginTop: 5,
        height: 40,
        width: 85,
        fontSize: 14,
        borderColor: 'gray',
        borderWidth: 0,
        backgroundColor: '#FFFFFF',
    },
    textResult: {
        marginTop: 5,
        height: 40,
        width: 128,
        fontSize: 14,
        borderColor: 'gray',
        borderWidth: 0,
        backgroundColor: '#FF992A',
        justifyContent: 'center',
        alignItems: 'center',
    },
    
    buttonMenu: {
        borderColor: 'transparent',
        backgroundColor: '#005f27',
        marginTop: 15,
        marginBottom: 12,
        height: 40,
        width: 275,
        justifyContent: 'center',
        alignSelf: 'center',
        textAlign: 'center'
    },
    buttonMenuLista: {
        borderColor: 'transparent',
        backgroundColor: '#005f27',
        marginTop: 10,
        marginBottom: 10,
        height: 40,
        width: 275,
        justifyContent: 'center',
        alignSelf: 'center',
        textAlign: 'center'
    },
    textButtonMenu: {
        fontSize: 14,
        color: '#FFFFFF'
    },
    customSeparatorText: {
        marginTop: 15,
    },
    smallLabel: {
        fontSize: 12
    }

}
export default Style;