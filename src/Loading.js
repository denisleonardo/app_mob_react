// Loading.js
import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native'
import './initializeApp';
import auth from '@react-native-firebase/auth';
import { connect } from 'react-redux';
import { addUserName } from './actions/user';

class Loading extends React.Component {

    componentDidMount() {
        auth().onAuthStateChanged(user => {
            this.props.navigation.navigate(user ? 'MainMenu' : 'Login')
            if (user) {
                this.props.add(user._user.email);
            }
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Loading</Text>
                <ActivityIndicator size="large" />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

const mapStateToProps = state => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        add: (name) => {
            dispatch(addUserName(name))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Loading)
