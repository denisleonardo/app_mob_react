import {
    ADD_PONTAS
} from '../actions/types';

const initicialState = {
    modelo: '',
    tamanho: '',
    angulo: ''
}

const pontasReducer = (state = initicialState, action) => {
    switch (action.type) {
        case ADD_PONTAS:
            return {
                ...state,
                modelo: action.modelo,
                tamanho: action.tamanho,
                angulo: action.angulo
            }
        default:
            return state;
    }
}

export default pontasReducer;