import {
    ADD_VARIAVEIS
} from '../actions/types';

const initicialState = {
    vazao: 0,
    volume: 0
}

const variaveisReducer = (state = initicialState, action) => {
    switch (action.type) {
        case ADD_VARIAVEIS:
            return {
                ...state,
                vazao: action.vazao,
                volume: action.volume
            }
        default:
            return state;
    }
}

export default variaveisReducer;