import {
    ADD_USER_NAME
} from '../actions/types';

const initicialState = {
    name: ''
}

const userReducer = (state = initicialState, action) => {
    switch (action.type) {
        case ADD_USER_NAME:
            return {
                ...state,
                name: action.payload
            }
        default:
            return state;
    }
}

export default userReducer;