import {
    ADD_VELOCIDADE
} from '../actions/types';

const initicialState = {
    resultado:0
}

const velocidadeReducer = (state = initicialState, action) => {
    switch (action.type) {
        case ADD_VELOCIDADE:
            return {
                ...state,
                resultado: action.payload
            }
        default:
            return state;
    }
}

export default velocidadeReducer;