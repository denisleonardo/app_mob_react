import React from 'react'
import { StyleSheet, Text, TextInput, View, Image, TouchableHighlight } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Styles, Form, Grid, UserLogin } from '../Styles';
import { Button, Toast } from 'native-base';
import auth from '@react-native-firebase/auth';
import '../initializeApp';

export default class SignUp extends React.Component {
  state = { email: '', password: '', errorMessage: null }
  handleSignUp = async () => {
    await auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => this.props.navigation.navigate('Home'))
      .catch(error => this.setState({ errorMessage: error.message }))
  }
  render() {
    return (
      <View style={Grid.wrapper}>
        <View style={Grid.row}>
          <View style={Grid.col}>
            <Image source={require('../img/logo.png')} style={Styles.logoScreenLogin} />
          </View>
        </View>
        <View style={Grid.row}>
          <View style={Grid.col}>
            <Text style={{ textAlign: 'center' }}>Digite um e-mail e uma senha para fazer o seu cadastro.</Text>
          </View>
        </View>
        <View style={Grid.row}>
          <View style={UserLogin.fieldset}>
            <TextInput
              style={UserLogin.input}
              placeholder="E-mail"
              onChangeText={email => this.setState({ email })}
              value={this.state.email}
              underlineColorAndroid="transparent"
            />
            <Icon style={UserLogin.icon} name="user" size={20} color="#000" />
          </View>
        </View>
        <View style={Grid.row}>
          <View style={UserLogin.fieldset}>
            <TextInput
              style={UserLogin.input}
              placeholder="Senha"
              onChangeText={password => this.setState({ password })}
              value={this.state.password}
              underlineColorAndroid="transparent"
            />
            <Icon style={UserLogin.icon} name="lock" size={20} color="#000" />
          </View>
        </View>
        <View style={Grid.row}>
          <View style={Grid.col}>
            {this.state.errorMessage &&
              <Text style={{ color: 'red' }}>
                {this.state.errorMessage}
              </Text>}
          </View>
        </View>
        <View style={Grid.row}>
          <View style={Grid.col}>
            <Button style={Form.button} onPress={this.handleSignUp}>
              <Text style={Form.buttonText}>CRIAR CONTA</Text>
            </Button>
            <TouchableHighlight onPress={() => this.props.navigation.navigate('Login')}>
              <Text style={Form.buttonTextLink}>Já possuo uma conta.</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8
  }
})