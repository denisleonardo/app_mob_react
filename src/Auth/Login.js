import React from 'react';
import { StyleSheet, Text, TextInput, View, Image, TouchableHighlight } from 'react-native';
import { Button, Toast } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Styles, Form, Grid, UserLogin } from '../Styles';
import '../initializeApp';
import auth from '@react-native-firebase/auth';
import { LoginManager, AccessToken, LoginButton } from 'react-native-fbsdk';


export default class Login extends React.Component {
    state = { email: '', password: '', errorMessage: null }

    handleLogin = () => {
        //Validar campos
        if (this.state.email == "") {
            Toast.show({
                text: 'E-mail em branco.',
                position: "top",
                buttonText: 'Ok'
            })
        } else if (this.state.password == "") {
            Toast.show({
                text: 'Você deve digitar uma senha!',
                position: "top",
                buttonText: 'Ok'
            })
        } else {
            console.log('asdasd')
            auth()
                .signInWithEmailAndPassword(this.state.email, this.state.password)
                .then(() => this.props.navigation.navigate('MainMenu'))
                .catch(error => this.setState({ errorMessage: error.message }))
        }
    }


    render() {
        return (
            <View style={Grid.wrapper}>
                <View style={Grid.row}>
                    <View style={Grid.col}>
                        <Image source={require('../img/logo.png')} style={Styles.logoScreenLogin} />
                    </View>
                </View>
                <View style={Grid.row}>
                    <View style={UserLogin.fieldset}>
                        <TextInput
                            style={UserLogin.input}
                            placeholder="E-mail"
                            onChangeText={email => this.setState({ email })}
                            underlineColorAndroid="transparent"
                        />
                        <Icon style={UserLogin.icon} name="user" size={20} color="#000" />
                    </View>
                </View>
                <View style={Grid.row}>
                    <View style={UserLogin.fieldset}>
                        <TextInput
                            style={UserLogin.input}
                            placeholder="Senha"
                            onChangeText={password => this.setState({ password })}
                            underlineColorAndroid="transparent"
                        />
                        <Icon style={UserLogin.icon} name="lock" size={20} color="#000" />
                    </View>
                </View>
                <View style={Grid.row}>
                    <View style={Grid.col}>
                        {this.state.errorMessage &&
                            <Text style={{ color: 'red' }}>
                                {this.state.errorMessage}
                            </Text>
                        }
                    </View>
                </View>
                <View style={Grid.row}>
                    <View style={Grid.col}>
                        <Button style={Form.button} onPress={this.handleLogin}>
                            <Text style={Form.buttonText}>ENTRAR</Text>
                        </Button>
                        <TouchableHighlight onPress={() => this.props.navigation.navigate('Signup')}>
                            <Text style={Form.buttonTextLink}>Cadastre-se</Text>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={Grid.row}>
                    <View style={Styles.hr} />
                </View>
                <View style={Grid.row}>
                    <View style={Grid.col}>
                        <Text style={{ marginBottom: 5 }}>Entrar usando:</Text>
                        <LoginButton
                            readPermissions={["public_profile", "email"]}
                            onLoginFinished={(error, result) => {
                                if (error) {

                                } else if (result.isCancelled) {

                                } else {
                                    AccessToken.getCurrentAccessToken()
                                        .then((data) => {
                                            // Create a new Firebase credential with the token
                                            const credential = auth.FacebookAuthProvider.credential(data.accessToken);
                                            // Login with the credential
                                            return auth().signInWithCredential(credential);
                                        })
                                        .then((user) => {
                                            this.props.navigation.navigate('Home')
                                        })
                                        .catch((error) => {
                                            const { code, message } = error;
                                            // For details of error codes, see the docs
                                            // The message contains the default Firebase string
                                            // representation of the error
                                        });
                                }
                            }}
                            onLogoutFinished={() => { console.log('Finished!') }} />
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonLogin: {
        height: 40,
        width: '90%',
        borderColor: 'gray',
        borderWidth: 1,
        marginTop: 8
    },
    textInput: {
        height: 40,
        width: '90%',
        borderColor: 'gray',
        borderWidth: 1,
        marginTop: 8
    }
})