//Store Reducer
import { createStore, combineReducers } from 'redux';
import userReducer from './reducers/userReducer';
import pontasReducer from './reducers/pontasReducer';
import variaveisReducer from './reducers/variaveisReducer';
import velocidadeReducer from './reducers/velocidadeReducer';

const rootReducer = combineReducers({
    user: userReducer,
    pontas: pontasReducer,
    variaveis: variaveisReducer,
    velocidade: velocidadeReducer
});

const configureStore = () => {
    return createStore(rootReducer);
}

export default configureStore;