import React, { Component } from 'react';
import { Text, Keyboard, Image, View, ImageBackground, TextInput, ScrollView, Picker } from 'react-native';
import { Body, Right, Button, Left, Header } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import Style from '../Style';
import NumberInput from '../Elements/NumberInput';

import { Pontas, ModeloPontas } from './tabelas';
import { Styles, Form, Grid } from '../Styles';
//Redux
import { connect } from 'react-redux';

class Calibracao extends Component {

    state = {
        textInput: [],
        inputData: [],
        constante_fluxometro: 560,
        vazao_esperada: 80,
        nova_constante: 0,
        vazao_coletada: 60
    }

    componentDidMount() {
        for (var i = 0; i <= 2; i++) {
            this.addTextInput((i));
        }
    }

    //function to add TextInput dynamically
    addTextInput = (index) => {
        let textInput = this.state.textInput;
        textInput.push(<TextInput placeholder={'Ponta ' + (index + 1)} key={index} style={Form.defaultPonta}
            onChangeText={(text) => this.addValues(text, index)} keyboardType={'numeric'} />);
        this.setState({ textInput });
    }
    //function to remove TextInput dynamically
    removeTextInput = () => {
        let textInput = this.state.textInput;
        let inputData = this.state.inputData;
        textInput.pop();
        inputData.pop();
        this.setState({ textInput, inputData });
    }
    //function to add text from TextInputs into single array
    addValues = (text, index) => {
        let dataArray = this.state.inputData;
        let checkBool = false;
        if (dataArray.length !== 0) {
            dataArray.forEach(element => {
                if (element.index === index) {
                    element.text = text;
                    checkBool = true;
                }
            });
        }
        if (checkBool) {
            this.setState({
                inputData: dataArray
            });
        }
        else {
            dataArray.push({ 'value': text, 'index': index });
            this.setState({
                inputData: dataArray
            });
        }
    }


    _keyboardDimiss = () => {
        Keyboard.dismiss();
    }

    _calcular = () => {
        let total = 0; //Vazao por ponta
        let media = 0;
        let total_pontas = this.state.inputData.length;
        let constante_fluxometro = this.state.constante_fluxometro;
        let vazao_coletada = this.state.vazao_coletada;
        let vazao_esperada = this.state.vazao_esperada;
        let nova_constante = 0;
        let vazao_medida = 0;

        //Soma toda a vazao
        this.state.inputData.map((item) => {
            total = total + parseInt(item.value);
        })
        //Calculo da media
        media = (total / total_pontas);
        //Calculo de vazao total
        vazao_total = (media * total_pontas);

        //Calculo da nova constante
        if (vazao_coletada > 0) {
            vazao_medida = vazao_coletada;
        } else {
            vazao_medida = vazao_total;
        }
        nova_constante = ((constante_fluxometro * vazao_esperada) / vazao_medida)
        nova_constante = Math.round(nova_constante);
        this.setState({nova_constante})

    }

    render() {
        const e = this.state.quantidade_pontas;
        return (
            <ImageBackground source={require('../img/background.jpg')} style={{ width: '100%', height: '100%' }}>
                <Header span style={Styles.header}>
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="chevron-left" size={14} color={'#FFFFFF'} />
                        </Button>
                    </Left>
                    <Body style={Styles.headerBody} >
                        <Image source={require('../img/logo-white.png')} style={Styles.logoImage} />
                        <Text style={Styles.h4}>Calibração</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="info-circle" size={16} color={'#FFFFFF'} />
                        </Button>
                    </Right>
                </Header>
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={Grid.wrapper}>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <TextInput style={Form.defaultSM} onChangeText={(vazao_coletada) => this.setState({ vazao_coletada })} value={this.state.vazao_coletada.toString()} keyboardType={'numeric'} />
                                <Text style={Form.label}>Vazão coletada</Text>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <TextInput style={Form.default} onChangeText={(constante_fluxometro) => this.setState({ constante_fluxometro })} value={this.state.constante_fluxometro.toString()} keyboardType={'numeric'} />
                                <Text style={Form.label}>Const. Fluxometro</Text>
                            </View>
                            <View style={Grid.col}>
                                <TextInput style={Form.default} onChangeText={(vazao_esperada) => this.setState({ vazao_esperada })} value={this.state.vazao_esperada.toString()} keyboardType={'numeric'} />
                                <Text style={Form.label}>Vazão Esperada</Text>
                            </View>
                        </View>
                        <View style={Grid.row2}>
                            {this.state.textInput.map((value, index) => {
                                return value
                            })}
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Button style={Form.button} onPress={() => this.addTextInput(this.state.textInput.length)}>
                                    <Text style={Form.buttonText}>Adicionar Ponta</Text>
                                </Button>
                            </View>
                            <View style={Grid.col}>
                                <Button style={Form.button} onPress={() => this.removeTextInput()}>
                                    <Text style={Form.buttonText}>Remover Ponta</Text>
                                </Button>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Button style={Form.button} onPress={() => this._calcular()}>
                                    <Text style={Form.buttonText}>CALCULAR</Text>
                                </Button>
                            </View>
                        </View>
                        <View style={Grid.row}>

                            <View style={Grid.col}>
                                <Text style={Form.textResultSM}>{this.state.nova_constante.toString()}</Text>
                                <Text style={Form.label}>Nova Constante</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground >
        );
    }
}

Calibracao.navigationOptions = {
    title: 'Calibração de Pontas',
}


const mapStateToProps = state => {
    return {
        state: state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // addVariaveis: (volume,vazao) => {
        //     dispatch(addVariaveis(volume,vazao))
        // },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Calibracao)
