import React, { Component } from 'react';
import { Text, Keyboard, Image, View, ImageBackground, TextInput, ScrollView, Picker } from 'react-native';
import { Body, Right, Button, Left, Header } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import Style from '../Style';
import { Pontas, ModeloPontas, Produtos } from './tabelas';
import { Styles, Form, Grid } from '../Styles';
//Redux
import { connect } from 'react-redux';

class ListarPontas extends Component {

    state = {
        produtos: Produtos
    }

    componentDidMount() {
        console.log(this.props.state);
    }
    s
    _keyboardDimiss = () => {
        Keyboard.dismiss();
    }

    render() {

        return (
            <ImageBackground source={require('../img/background.jpg')} style={{ width: '100%', height: '100%' }}>
                <Header span style={Styles.header}>
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="chevron-left" size={14} color={'#FFFFFF'} />
                        </Button>
                    </Left>
                    <Body style={Styles.headerBody} >
                        <Image source={require('../img/logo-white.png')} style={Styles.logoImage} />
                        <Text style={Styles.h4}>Lista de Pontas</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="info-circle" size={16} color={'#FFFFFF'} />
                        </Button>
                    </Right>
                </Header>
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View>
                        {Object.entries(this.state.produtos).map(([key, value]) => {
                            console.log(value.imagem);
                            
                            return (
                                <Image key={key} source={image} style={{ width: 150, height: 150 }} />
                            )
                        })}
                    </View>
                </ScrollView>
            </ImageBackground >
        );
    }
}

ListarPontas.navigationOptions = {
    title: 'Pontas',
}

const mapStateToProps = state => {
    return {
        state: state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // addVariaveis: (volume,vazao) => {
        //     dispatch(addVariaveis(volume,vazao))
        // },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListarPontas)
