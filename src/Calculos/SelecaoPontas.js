import React, { Component } from 'react';
import { Text, Keyboard, Image, View, ImageBackground, TextInput, ScrollView, Picker } from 'react-native';
import { Body, Right, Button, Left, Header } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import Style from '../Style';
import { Pontas, ModeloPontas } from './tabelas';
import { Styles, Form, Grid } from '../Styles';
import { addPontas } from '../actions/pontas';
//Redux
import { connect } from 'react-redux';

class SelecaoPontas extends Component {

    state = {
        modelos: ModeloPontas,
        pontas: Pontas,
        angulo: 110,
        tamanho_selecionado: '.02',
        modelo_selecionado: 'TT',
        modelo_info: [],
        ponta_info: [],
        status: ''
    }

    componentDidMount() {
        this.modeloGetData();
        this.pontaGetData();
    }

    //pega os dados da ponta selecionada
    modeloGetData = () => {
        let modelo_selecionado = this.state.modelo_selecionado;
        let modelos = this.state.modelos;
        filtered = Object.entries(modelos)
            .filter(item =>
                item[0] == modelo_selecionado
            ).map((item) => {
                this.setState({ modelo_info: item });
            });
    }

    pontaGetData = () => {
        let tamanho_selecionado = this.state.tamanho_selecionado;
        let pontas = this.state.pontas;
        filtered = Object.entries(pontas)
            .filter(item =>
                item[0] == tamanho_selecionado
            ).map((item) => {
                this.setState({ ponta_info: item });
            });
    }

    _keyboardDimiss = () => {
        Keyboard.dismiss();
    }

    _calcular = async () => {
        let ponta_info;
        let modelo_info;
        await this.modeloGetData();
        await this.pontaGetData();
        ponta_info = this.state.ponta_info;
        modelo_info = this.state.modelo_info;

        //Calcular se a pressao
        let bar = ponta_info[1].pressao;
        let bar_min = modelo_info[1].min;
        let bar_max = modelo_info[1].max;

        if (bar >= bar_min && bar <= bar_max) {
            this.setState({ status: 1 });
        } else {
            this.setState({ status: 0 });
        }

        //Adicionar ao store
        this.props.addPontas(this.state.tamanho_selecionado, this.state.modelo_selecionado, this.state.angulo)
    }

    render() {
        let conditional;
        if (this.state.status === 1) {
            conditional = (
                <View style={Grid.col}>
                    <Text style={Form.textResult}>Aplicável</Text>
                    <Button style={Form.button} onPress={() => navigation.navigate('ListarPontas')}>
                        <Text style={Form.buttonText}>Listar Pontas</Text>
                    </Button>
                </View>
            )
        } else if (this.state.status === 0) {
            (
                <View style={Grid.col}>
                    <Text style={Form.textResult}>Não Aplicável</Text>
                </View>
            )
        } else {
            condicional = <Text></Text>
        }
        const navigation = this.props.navigation;
        return (
            <ImageBackground source={require('../img/background.jpg')} style={{ width: '100%', height: '100%' }}>
                <Header span style={Styles.header}>
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="chevron-left" size={14} color={'#FFFFFF'} />
                        </Button>
                    </Left>
                    <Body style={Styles.headerBody} >
                        <Image source={require('../img/logo-white.png')} style={Styles.logoImage} />
                        <Text style={Styles.h4}>Seleção de Pontas</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="info-circle" size={16} color={'#FFFFFF'} />
                        </Button>
                    </Right>
                </Header>
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={Grid.wrapper}>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Text style={Form.label}>Modelo de Ponta {this.state.modelo_selecionado}</Text>
                                <Picker
                                    selectedValue={this.state.modelo_selecionado}
                                    style={Form.selectDefault}
                                    onValueChange={(itemValue, itemIndex) => {
                                        this.setState({ modelo_selecionado: itemValue })
                                    }}>
                                    {
                                        Object.entries(this.state.modelos).map(([key, value]) => {
                                            return (
                                                <Picker.Item key={key} style={Form.selectDefaultItem} label={key} value={key} />
                                            )
                                        })
                                    }
                                </Picker>
                            </View>
                            <View style={Grid.col}>
                                <Text style={Form.label}>Tamanho da Ponta</Text>
                                <Picker
                                    selectedValue={this.state.tamanho_selecionado}
                                    style={Form.selectDefault}
                                    onValueChange={(itemValue, itemIndex) => {
                                        this.setState({ tamanho_selecionado: itemValue })
                                    }}>
                                    {
                                        Object.entries(this.state.pontas).map(([key, value]) => {
                                            return (
                                                <Picker.Item style={Form.selectDefaultItem} key={key} label={key} value={key} />
                                            )
                                        })
                                    }
                                </Picker>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <TextInput style={Form.defaultSM} onChangeText={(angulo) => this.setState({ angulo })} value={this.state.angulo.toString()} keyboardType={'numeric'} />
                                <Text style={Form.label}>Ângulo</Text>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Button style={Form.button} onPress={() => this._calcular()}>
                                    <Text style={Form.buttonText}>VALIDAR</Text>
                                </Button>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                {conditional}
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground >
        );
    }
}

SelecaoPontas.navigationOptions = {
    title: 'Seleção de Pontas',
}

const mapStateToProps = state => {
    return {
        state: state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addPontas: (ponta, modelo, angulo) => {
            dispatch(addPontas(ponta, modelo, angulo))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SelecaoPontas)
