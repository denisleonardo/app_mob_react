import React, { Component } from 'react';
import { Text, Keyboard, Image, View, ImageBackground, TextInput, ScrollView, Picker } from 'react-native';
import { Body, Right, Button, Left, Header } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Styles, Form, Grid } from '../Styles';
//Redux
import { connect } from 'react-redux';
import { addVariaveis } from '../actions/variaveis';

class Variaveis extends Component {
    state = {
        calibracao: 1,
        pontas: 0,
        eficiencia_bomba: 0,
        velocidade: !this.props.state.velocidade.resultado ? 0 : this.props.state.velocidade.resultado,
        espacamento: 0,
        volume: 0,
        vazao: 0,
        vazao_desejada: 0,
        capacidade_bomba: 0,
        _calc_velocidade: 0,
        _calc_espacamento: 0,
        _calc_volume: 0,
        _calc_vazao: 0,
    }

    componentDidMount() {
        this.setState({ _calc_velocidade: this.state.velocidade });
        this.setState({ _calc_espacamento: this.state.espacamento });
        this.setState({ _calc_volume: this.state.volume });
        this.setState({ _calc_vazao: this.state.vazao });
    }

    _keyboardDimiss = () => {
        Keyboard.dismiss();
    }

    _calcular = () => {
        this._keyboardDimiss();
        let calibracao = this.state.calibracao;
        let pontas = this.state.pontas;
        let eficiente_bomba = this.state.eficiencia_bomba;
        let _velocidade = this.state._calc_velocidade;
        let _espacamento = this.state._calc_espacamento;
        let _volume = this.state._calc_volume;
        let _vazao = this.state._calc_vazao;

        console.log(calibracao);

        //Calcula os outros valores caso não tenha sido passado
        if (_velocidade <= 0 || _velocidade == "") {
            let r = ((_vazao * 600) / (_volume * _espacamento));
            if (Number.isNaN(r)) {
                r = 0;
            }
            _velocidade = r;
            this.setState({ velocidade: Math.abs(r).toFixed(2) });
            this.setState({ _calc_velocidade: r });
        } else {
            this.setState({ _calc_velocidade: _velocidade });
        }

        if (_espacamento <= 0 || _espacamento == "") {

            let r = ((_vazao * 600) / (_volume * _velocidade));
            if (Number.isNaN(r)) {
                r = 0;
            }
            _espacamento = r;
            this.setState({ espacamento: Math.abs(r).toFixed(2) });
            this.setState({ _calc_espacamento: r });
        } else {
            this.setState({ _calc_espacamento: _espacamento });
        }

        if (_volume <= 0 || _volume == "") {
            let r = ((_vazao * 600) / (_espacamento * _velocidade));
            if (Number.isNaN(r)) {
                r = 0;
            }
            _volume = r;
            this.setState({ volume: Math.abs(r).toFixed(2) });
            this.setState({ _calc_volume: r });
        } else {
            this.setState({ _calc_volume: _volume });
        }

        //Sempre calcula a vazão
        let r = ((_espacamento * _volume * _velocidade) / 600);
        if (Number.isNaN(r)) {
            r = 0;
        }
        _vazao = r;
        this.setState({ vazao: Math.abs(r).toFixed(2) });
        this.setState({ _calc_vazao: r });

        //Calcular a vazao desejada
        var calc_vazao;
        if (calibracao == 1) {
            if (_vazao > 0) {
                calc_vazao = (_vazao / 1);
            } else {
                calc_vazao = 0;
            }

            this.setState({ vazao_desejada: Math.abs(calc_vazao).toFixed(2) });
        } else {
            if (_vazao > 0) {
                calc_vazao = (_vazao / pontas);
            } else {
                calc_vazao = 0;
            }
            this.setState({ vazao_desejada: Math.abs(calc_vazao).toFixed(2) });
        }

        //Calcular a capacidade da Bomba
        var capacidade = (calc_vazao * pontas / (eficiente_bomba / 100));
        console.log(capacidade);
        if (Number.isNaN(capacidade)) {
            this.setState({ capacidade_bomba: Math.abs(0).toFixed(2) });
        } else {
            this.setState({ capacidade_bomba: Math.abs(capacidade).toFixed(2) });
        }

        this.props.addVariaveis(this.state.volume, this.state.vazao_desejada);

    }
    _changeState = (e, el1, el2) => {
        //Salva o valor padrao com fixed
        let fixed = '{"' + el1 + '":"' + e + '"}';
        fixed = JSON.parse(fixed);
        this.setState(fixed);
        //Salva o valor padrao sem fixed
        let calc = '{"' + el2 + '":"' + e + '"}';
        calc = JSON.parse(calc);
        this.setState(calc);
    }

    render() {
        const { params } = this.props.navigation.state;
        return (
            <ImageBackground source={require('../img/background.jpg')} style={{ width: '100%', height: '100%' }}>
                <Header span style={Styles.header}>
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="chevron-left" size={14} color={'#FFFFFF'} />
                        </Button>
                    </Left>
                    <Body style={Styles.headerBody} >
                        <Image source={require('../img/logo-white.png')} style={Styles.logoImage} />
                        <Text style={Styles.h4}>Cálculo de Variáveis</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="info-circle" size={16} color={'#FFFFFF'} />
                        </Button>
                    </Right>
                </Header>
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={Grid.wrapper}>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Text style={Form.label}>Tipo de Calibração</Text>
                                <Picker
                                    selectedValue={this.state.calibracao}
                                    style={Form.selectDefault}
                                    onValueChange={(itemValue, itemIndex) =>
                                        this.setState({ calibracao: itemValue })
                                    }>
                                    <Picker.Item style={Form.selectDefaultItem} label="Calibração de Barra" value="1" />
                                    <Picker.Item style={Form.selectDefaultItem} label="Calibração de Turbo" value="2" />
                                </Picker>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <TextInput onFocus={() => this.setState({ pontas: '' })} style={Form.default} onChangeText={(pontas) => this.setState({ pontas })} value={this.state.pontas.toString()} keyboardType={'numeric'} />
                                <Text style={Form.label}>nº de Pontas</Text>
                            </View>
                            <View style={Grid.col}>
                                <TextInput onFocus={() => this.setState({ eficiencia_bomba: '' })} style={Form.default} onChangeText={(eficiencia_bomba) => this.setState({ eficiencia_bomba })} value={this.state.eficiencia_bomba.toString()} keyboardType={'numeric'} />
                                <Text style={Form.label}>Efic. Bomba (%)</Text>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <TextInput onFocus={() => this.setState({ velocidade: '' })} style={Form.default} onChangeText={(val) => this._changeState(val, 'velocidade', '_calc_velocidade')} value={this.state.velocidade.toString()} keyboardType={'numeric'} />
                                <Text style={Form.label}>Velocidade (km/h)</Text>
                            </View>
                            <View style={Grid.col}>
                                <TextInput onFocus={() => this.setState({ espacamento: '' })} style={Form.default} onChangeText={(val) => this._changeState(val, 'espacamento', '_calc_espacamento')} value={this.state.espacamento.toString()} keyboardType={'numeric'} />
                                <Text style={Form.label}>Espaçamento (m)</Text>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <TextInput onFocus={() => this.setState({ volume: '' })} style={Form.default} onChangeText={(val) => this._changeState(val, 'volume', '_calc_volume')} value={this.state.volume.toString()} keyboardType={'numeric'} />
                                <Text style={Form.label}>Volume (L/há)</Text>
                            </View>
                            <View style={Grid.col}>
                                <TextInput onFocus={() => this.setState({ vazao: '' })} style={Form.default} onChangeText={(val) => this._changeState(val, 'vazao', '_calc_vazao')} value={this.state.vazao.toString()} keyboardType={'numeric'} />
                                <Text style={Form.label}>Vazão (L/min)</Text>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Form.hr}></View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Text style={Form.textResult}>{this.state.vazao_desejada.toString()}</Text>
                                <Text style={Form.label}>Vaz. Desj. (L/min)</Text>
                            </View>
                            <View style={Grid.col}>
                                <Text style={Form.textResult}>{this.state.capacidade_bomba.toString()}</Text>
                                <Text style={Form.label}>Bomba (L/min)</Text>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Button style={Form.button} onPress={() => this._calcular()}>
                                    <Text style={Form.buttonText}>CALCULAR</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground >
        );
    }
}

Variaveis.navigationOptions = {
    title: 'Variáveis',
}


const mapStateToProps = state => {
    return {
        state: state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addVariaveis: (volume, vazao) => {
            dispatch(addVariaveis(volume, vazao))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Variaveis)
