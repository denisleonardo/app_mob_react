import React, { Component } from 'react';
import { Text, Keyboard, Image, View, ImageBackground } from 'react-native';
import {
    Content, Body, Title, Right, Button, Left,
    Footer, FooterTab,
    Item, Picker, Label, Input, Icon, Header
}
    from 'native-base';
import NumberInput from '../../Elements/NumberInput';
import { connect } from 'react-redux';
import {addPontas} from '../../actions/pontas';


const style = {
    header: {
        backgroundColor: '#005f27',
        color: '#FFFFFF'
    },
    titleColor: {
        color: '#FFFFFF'
    },
    formControl: {
        width: 120,
        textAlign: 'left',
        fontSize: 14,
        marginBottom: 10,
        backgroundColor: '#FFFFFF'

    },
    label: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 40
    }
}
class Pontas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textInput: []
        }
    }
    addTextInput = (key) => {
        let textInput = this.state.textInput;
        textInput.push(
            <View key={key}>
                <Text>Ponta {key + 1}</Text>
                <NumberInput style={style.formControl} onChangeText={(volume) => this.setState({ volume })} value={this.state.volume} keyboardType={'numeric'} key={key} />
            </View>
        );
        this.props.add(textInput);
        this.setState({ textInput })
    }

    _keyboardDimiss = () => {
        Keyboard.dismiss();
    }
    //Criar um STATE para os elementos na funcao
    _changeState = (e, el1, el2) => {
        //Salva o valor padrao com fixed
        let fixed = '{"' + el1 + '":"' + e + '"}';
        fixed = JSON.parse(fixed);
        this.setState(fixed);

        //Salva o valor padrao sem fixed
        let calc = '{"' + el2 + '":"' + e + '"}';
        calc = JSON.parse(calc);
        this.setState(calc);
    }

    render() {
        const inputs = this.state.textInput;
        return (
            <Content>
                <Image source={require('../../img/logo.png')} style={Style.logoAllScreen} />
                {inputs.map((value, index) => {
                    return value
                })}
                <Button style={Style.buttonMenuLista} onPress={() => this.addTextInput(this.state.textInput.length)}>
                    <Text style={Style.textButtonMenu}>Adicionar Pontas</Text>
                </Button>
            </Content>
        );
    }
}



const mapStateToProps = state => {
    return {
        state: state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        add: (pontas) => {
            dispatch(addPontas(pontas))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pontas)