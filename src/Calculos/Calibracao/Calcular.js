import React, { Component } from 'react';
import { Text, Keyboard, Image, View, ImageBackground } from 'react-native';
import {
    Content, Body, Title, Right, Button, Left,
    Footer, FooterTab,
    Item, Picker, Label, Input, Icon, Header
}
    from 'native-base';
import { connect } from 'react-redux';
import { addPontas } from '../../actions/pontas';

class Calcular extends Component {

    componentDidMount() {

    }

    _keyboardDimiss = () => {
        Keyboard.dismiss();
    }
    //Criar um STATE para os elementos na funcao
    _changeState = (e, el1, el2) => {
        //Salva o valor padrao com fixed
        let fixed = '{"' + el1 + '":"' + e + '"}';
        fixed = JSON.parse(fixed);
        this.setState(fixed);

        //Salva o valor padrao sem fixed
        let calc = '{"' + el2 + '":"' + e + '"}';
        calc = JSON.parse(calc);
        this.setState(calc);
    }

    render() {
        return (
            <Content>
                <Text>Cacular</Text>
                {this.props.state.pontas.pontas.map((data) => {
                    return data
                })}
            </Content>
        );
    }
}


const mapStateToProps = state => {
    return {
        state: state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        add: (pontas) => {
            dispatch(addPontas(pontas))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Calcular)