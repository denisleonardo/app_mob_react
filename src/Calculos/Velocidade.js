import React, { Component } from 'react';
import { Text, Keyboard, Image, View, ImageBackground, TouchableHighlight, ScrollView } from 'react-native';
import { Body, Right, Button, Left, Header } from 'native-base';
import { Styles, Grid, Form, Cron } from '../Styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Stopwatch } from 'react-native-stopwatch-timer'
import NumberInput from '../Elements/NumberInput';
//Redux
import { connect } from 'react-redux';
import { addVelocidade } from '../actions/velocidade';


class Velocidade extends Component {
    state = {
        actual_time: '',
        distancia: "100",
        resultado: "0",
        stopwatchStart: false,
        stopwatchReset: false,
        startTime: 0,
        horas: 0,
        minutos: 0,
        segundos: 0
    }

    componentDidMount(){
        console.log(this.props.state);
    }

    resetTimer = () => {
        this.setState({ timerStart: false, timerReset: true });
    }

    toggleStopwatch = () => {
        this.setState({ stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false });
    }

    resetStopwatch = () => {
        this.setState({ stopwatchStart: false, stopwatchReset: true });
    }

    getFormattedTime = (time) => {
        this.currentTime = time;
    };

    _keyboardDimiss = () => {
        Keyboard.dismiss();
    }

    _calcular = () => {
        
        let current_time = this.currentTime;
        let dis = this.state.distancia;
        let t = current_time.split(':');

        //Separacao do timer
        this.setState({ horas: t[0] });
        this.setState({ minutos: t[1] });
        this.setState({ segundos: t[2] });

        let horas_segundos = ((t[0] * 60) * 60);
        let minutos_segundos = (t[1] * 60);
        let segundos_total = horas_segundos + minutos_segundos + t[2];

        let resultado = ((3.6 * dis) / (segundos_total / 24)) / 24;

        resultado = Math.abs(resultado).toFixed(2);
        this.setState({ resultado: resultado });

        this.addVel(resultado);
    }

    addVel = (state) =>{
        this.props.addVelocidade(state);
    }

    _changeState = (e, el1) => {
        //Salva o valor padrao sem fixed
        let calc = '{"' + el1 + '":"' + e + '"}';
        calc = JSON.parse(calc);
        this.setState(calc);
    }

    render() {
        return (
            <ImageBackground source={require('../img/background.jpg')} style={{ width: '100%', height: '100%' }}>
                <Header span style={Styles.header}>
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="chevron-left" size={14} color={'#FFFFFF'} />
                        </Button>
                    </Left>
                    <Body style={Styles.headerBody} >
                        <Image source={require('../img/logo-white.png')} style={Styles.logoImage} />
                        <Text style={Styles.h4}>Velocidade</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="info-circle" size={16} color={'#FFFFFF'} />
                        </Button>
                    </Right>
                </Header>
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={Grid.wrapper}>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Text style={Form.label}>Dist. (m):</Text>
                                <NumberInput style={Form.defaultSM} onChangeText={(distancia) => this.setState({ distancia })} value={this.state.distancia} keyboardType={'numeric'} />
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Text style={Form.label}>Cronômetro</Text>
                                <Stopwatch laps msecs start={this.state.stopwatchStart}
                                    reset={this.state.stopwatchReset}
                                    options={Cron}
                                    msecs={true}
                                    getTime={this.getFormattedTime}
                                    startTime={this.state.startTime}
                                />
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.colRow}>
                                <TouchableHighlight style={Form.buttonXS} onPress={this.toggleStopwatch}>
                                    <Icon style={Form.buttonText} name={!this.state.stopwatchStart ? "play-circle" : "stop-circle"} size={20} color={'#FFFFFF'} />
                                </TouchableHighlight>
                                <TouchableHighlight style={Form.buttonXS} onPress={this.resetStopwatch}>
                                    <Icon style={Form.buttonText} name="undo" size={20} color={'#FFFFFF'} />
                                </TouchableHighlight>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Button style={Form.button} onPress={() => this._calcular()}>
                                    <Text style={Form.buttonText}>CALCULAR</Text>
                                </Button>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <View style={Styles.result}>
                                    <Text style={{ fontSize: 30 }}>{this.state.resultado} Km/h</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}

Velocidade.navigationOptions = {
    title: 'Velocidade',
}

const mapStateToProps = state => {
    return {
        state: state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addVelocidade: (data) => {
            dispatch(addVelocidade(data))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Velocidade)
