export const Produtos = {
    'TT11001': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TT',
        angulo: '110',
        imagem: '../img/pontas/TT11001.jpg'
    },
    'TT110015': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TT',
        angulo: '110',
        imagem: '../img/pontas/TT110015.jpg'
    },
    'TT11002': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TT',
        angulo: '110',
        imagem: '../img/pontas/TT11002.jpg'
    },
    'TT110025': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TT',
        angulo: '110',
        imagem: '../img/pontas/TT110025.jpg'
    },
    'TT11003': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TT',
        angulo: '110',
        imagem: '../img/pontas/TT11003.jpg'
    },
    'TT11004': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TT',
        angulo: '110',
        imagem: '../img/pontas/TT11004.jpg'
    },
    'TT11005': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TT',
        angulo: '110',
        imagem: '../img/pontas/TT11005.jpg'
    },
    'TT11006': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TT',
        angulo: '110',
        imagem: '../img/pontas/TT11006.jpg'
    },
    'TT11008': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TT',
        angulo: '110',
        imagem: '../img/pontas/TT11008.jpg'
    },
    'TJ11002': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TJ',
        angulo: '110',
        imagem: '../img/pontas/TJ11002.jpg'
    },
    'TJ110025': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TJ',
        angulo: '110',
        imagem: '../img/pontas/TJ110025.jpg'
    },
    'TJ11003': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TJ',
        angulo: '110',
        imagem: '../img/pontas/TJ11003.jpg'
    },
    'TJ11004': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TJ',
        angulo: '110',
        imagem: '../img/pontas/TJ11004.jpg'
    },
    'TJ11005': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TJ',
        angulo: '110',
        imagem: '../img/pontas/TJ11005.jpg'
    },
    'TJ11006': {
        descricao: 'Descricao basica da ponta',
        tipo: 'TJ',
        angulo: '110',
        imagem: '../img/pontas/TJ11006.jpg'
    },
};

export const ModeloPontas = {
    'P': { min: 0.98, max: 10.09 },
    'XR': { min: 0.98, max: 4.09 },
    'DG': { min: 1.95, max: 4.09 },
    'TT': { min: 0.98, max: 6.09 },
    'TJ': { min: 1.95, max: 4.09 },
    'AI': { min: 1.95, max: 8.09 },
    'TF': { min: 0.98, max: 3.09 },
    'TX': { min: 2.95, max: 10.09 }
};

export const Pontas = {
    '.0050': {
        fator: 8.838835,
        pressao: 59.8,
        dmv: 0
    },
    '.0067': {
        fator: 6.77596356818118,
        pressao: 35.2,
        dmv: 0
    },
    '.01': {
        fator: 4.41941738241592,
        pressao: 15.0,
        dmv: 243.221148027563
    },
    '.015': {
        fator: 2.94627825494395,
        pressao: 6.6,
        dmv: 275.52303491299
    },
    '.02': {
        fator: 2.20970869120796,
        pressao: 3.7,
        dmv: 306.182222088867
    },
    '.025': {
        fator: 1.74594266959641,
        pressao: 2.3,
        dmv: 0
    },
    '.03': {
        fator: 1.45795212615783,
        pressao: 1.6,
        dmv: 349.875567585336
    },
    '.04': {
        fator: 1.09628958323496,
        pressao: 0.9,
        dmv: 403.576763430331
    },
    '.05': {
        fator: 0.878393517001922,
        pressao: 0.6,
        dmv: 424.024928473098
    },
    '.06': {
        fator: 0.732753141125956,
        pressao: 0.4,
        dmv: 0
    },
    '.08': {
        fator: 0.548144791617479,
        pressao: 0.2,
        dmv: 0
    },
    '.10': {
        fator: 0.439196758500961,
        pressao: 0.1,
        dmv: 0
    },
    '.15': {
        fator: 0.292797839000641,
        pressao: 0.1,
        dmv: 0
    },
    '.20': {
        fator: 0.219598379250481,
        pressao: 0.1,
        dmv: 0
    },
    '2': {
        fator: 1.09628958323496,
        pressao: 0.9,
        dmv: 0
    },
    '2.5': {
        fator: 0.878393517001922,
        pressao: 0.6,
        dmv: 0
    },
    '3': {
        fator: 0.732753141125956,
        pressao: 0.4,
        dmv: 0
    },
    '4': {
        fator: 0.548144791617479,
        pressao: 0.2,
        dmv: 0
    },
    '5': {
        fator: 0.439196758500961,
        pressao: 0.1,
        dmv: 0
    },
    '7.5': {
        fator: 0.292797839000641,
        pressao: 0.1,
        dmv: 0
    },
    '10': {
        fator: 0.219257916646991,
        pressao: 0.0,
        dmv: 0
    },
    '.3': {
        fator: 0,
        pressao: 86.7,
        dmv: 0
    },
    '.4': {
        fator: 0,
        pressao: 41.5,
        dmv: 0
    },
    '.6': {
        fator: 0,
        pressao: 17.3,
        dmv: 0
    },
    '.8': {
        fator: 0,
        pressao: 8.7,
        dmv: 0
    },
    '.10': {
        fator: 0,
        pressao: 5.5,
        dmv: 0
    },
    '.12': {
        fator: 0,
        pressao: 3.7,
        dmv: 0
    },
    '.18': {
        fator: 0,
        pressao: 1.6,
        dmv: 0
    }
};