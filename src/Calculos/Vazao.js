import React, { Component } from 'react';
import { Text, Keyboard, Image, View, ImageBackground, ScrollView, TouchableOpacity } from 'react-native';
import { Body, Right, Button, Left, Header } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Stopwatch } from 'react-native-stopwatch-timer'

import Style from '../Style';
import { Styles, Form, Grid, Cron } from '../Styles';
import NumberInput from '../Elements/NumberInput';

import { connect } from 'react-redux';

const style = {
    header: {
        backgroundColor: '#005f27',
        color: '#FFFFFF'
    },
    titleColor: {
        color: '#FFFFFF'
    },
    formControl: {
        width: 60,
        textAlign: 'left',
        fontSize: 14,
        borderTopWidth: 2,
        borderBottomtWidth: 2
    },
    label: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 40
    }
}

class Vazao extends Component {
    state = {
        vazao: !this.props.state.variaveis.vazao == 0 ? 0 : this.props.state.variaveis.vazao,
        volume: "0",
        vazao_real: "0",
        erro: "0",
        stopwatchStart: false,
        stopwatchReset: false,
        startTime: 0,
        horas: 0,
        minutos: 0,
        segundos: 0
    }

    componentDidMount() {
        console.log(this.props.state.variaveis.vazao);
        // console.log(this.props.state.variaveis.info)
        // if (this.props.state.variaveis.info.vazao_desejada != 'undefined') {
        //     console.log('asdasda');
        //     this.setState({ vazao: this.props.state.variaveis.info.vazao_desejada })
        // }
    }

    resetTimer = () => {
        this.setState({ timerStart: false, timerReset: true });
    }

    toggleStopwatch = () => {
        this.setState({ stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false });
    }

    resetStopwatch = () => {
        this.setState({ stopwatchStart: false, stopwatchReset: true });
    }

    getFormattedTime = (time) => {
        this.currentTime = time;
    };

    _keyboardDimiss = () => {
        Keyboard.dismiss();
    }

    _calcular = () => {
        let current_time = this.currentTime;
        let t = current_time.split(':');

        //Separacao do timer
        this.setState({ horas: t[0] });
        this.setState({ minutos: t[1] });
        this.setState({ segundos: t[2] });

        let horas_segundos = ((t[0] * 60) * 60);
        let minutos_segundos = (t[1] * 60);
        let segundos_total = horas_segundos + minutos_segundos + t[2];

        let volume = this.state.volume;
        let vazao = this.state.vazao;

        let b9 = (segundos_total / 60);

        //Calcula a vazao real
        
        let vazao_real = (volume / (b9 * 1000));
        this.setState({ vazao_real: vazao_real.toFixed(2).toString() })

        arr_vazao_real = Math.round(vazao_real * 100) / 100

        //Calcula o erro
        let erro = (vazao / arr_vazao_real);

        if (erro >= 1) {
            erro = (erro - 1);
        }
        if (erro != 'NaN') {
            this.setState({ erro: (erro * 100).toFixed(2) });
        } else {
            this.setState({ erro: 0 });
        }


    }

    _changeState = (e, el1) => {
        //Salva o valor padrao sem fixed
        let calc = '{"' + el1 + '":"' + e + '"}';
        calc = JSON.parse(calc);
        this.setState(calc);
    }

    render() {
        return (
            <ImageBackground source={require('../img/background.jpg')} style={{ width: '100%', height: '100%' }}>
                <Header span style={Styles.header}>
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="chevron-left" size={14} color={'#FFFFFF'} />
                        </Button>
                    </Left>
                    <Body style={Styles.headerBody} >
                        <Image source={require('../img/logo-white.png')} style={Styles.logoImage} />
                        <Text style={Styles.h4}>Vazão de Ponta</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignItems: 'flex-end' }}>
                            <Icon name="info-circle" size={16} color={'#FFFFFF'} />
                        </Button>
                    </Right>
                </Header>
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={Grid.wrapper}>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Text style={Form.label}>Cronômetro</Text>
                                <Stopwatch laps msecs start={this.state.stopwatchStart}
                                    reset={this.state.stopwatchReset}
                                    options={Cron}
                                    msecs={true}
                                    getTime={this.getFormattedTime}
                                    startTime={this.state.startTime}
                                />
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.colRow}>
                                <TouchableOpacity style={Form.buttonXS} onPress={this.toggleStopwatch}>
                                    <Icon style={Form.buttonText} name={!this.state.stopwatchStart ? "play-circle" : "stop-circle"} size={20} color={'#FFFFFF'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={Form.buttonXS} onPress={this.resetStopwatch}>
                                    <Icon style={Form.buttonText} name="undo" size={20} color={'#FFFFFF'} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <NumberInput style={Form.default} onChangeText={(vazao) => this.setState({ vazao })} value={this.state.vazao} keyboardType={'numeric'} />
                                <Text style={Form.label}>Vazão Desejada</Text>
                            </View>
                            <View style={Grid.col}>
                                <NumberInput style={Form.default} onChangeText={(volume) => this.setState({ volume })} value={this.state.volume} keyboardType={'numeric'} />
                                <Text style={Form.label}>Vol. Coletado (ml):</Text>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Text style={Form.textResult}>{this.state.vazao_real} L/min</Text>
                                <Text style={Form.label}>Vazão Real</Text>
                            </View>
                            <View style={Grid.col}>
                                <Text style={Form.textResult}>{this.state.erro} %</Text>
                                <Text style={Form.label}>Erro</Text>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <Button style={Form.button} onPress={() => this._calcular()}>
                                    <Text style={Form.buttonText}>CALCULAR</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}

Vazao.navigationOptions = {
    title: 'Vazão da Ponta',
}



const mapStateToProps = state => {
    return {
        state: state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        add: (name) => {
            dispatch(addUserName(name))
        },
        get: () => {
            return dispatch(getUserName())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Vazao)