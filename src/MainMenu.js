import React, { Component } from 'react';
import { Image, Text, StatusBar, ImageBackground, View, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import { Body, Right, Button, Left, Header } from 'native-base';
import { Styles, Grid, Form } from './Styles';
import Icon from 'react-native-vector-icons/FontAwesome';

import './initializeApp';
import auth from '@react-native-firebase/auth';
import { LoginManager } from 'react-native-fbsdk';
import { connect } from 'react-redux';
import { addUserName } from './actions/user';

class MainMenu extends Component {
    logout = () => {
        auth().signOut()
            .then(() => { LoginManager.logOut() })
            .then(() => {
                this.props.navigation.navigate('Loading')
            });
    }
    render() {
        const navigation = this.props.navigation;
        return (
            <ImageBackground source={require('./img/background.jpg')} style={{ width: '100%', height: '100%' }}>
                <Header span style={Styles.header}>
                    <Left style={{ flex: 1 }} />
                    <Body style={Styles.headerBody} >
                        <Image source={require('./img/logo-white.png')} style={Styles.logoImage} />
                    </Body>
                    <Right style={{ flex: 1 }} />
                </Header>
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                    <View style={Grid.wrapper}>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <TouchableOpacity ref="touch" style={Styles.touchMenuBtn} onPress={() => navigation.navigate('Variaveis')}>
                                    <View style={Styles.menuBtn}>
                                        <View style={Styles.menuBtnInner}>
                                            <Image style={Styles.iconBtn} source={require('./img/icons/variaveis.png')} />
                                        </View>
                                        <Text style={Styles.textBtn}>Variáveis</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={Grid.col}>
                                <TouchableOpacity ref="touch" style={Styles.touchMenuBtn} onPress={() => navigation.navigate('Velocidade')}>
                                    <View style={Styles.menuBtn}>
                                        <View style={Styles.menuBtnInner}>
                                            <Image style={Styles.iconBtn} source={require('./img/icons/velocidade.png')} />
                                        </View>
                                        <Text style={Styles.textBtn}>Velocidade</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={Grid.col}>
                                <TouchableOpacity ref="touch" style={Styles.touchMenuBtn} onPress={() => navigation.navigate('Vazao')}>
                                    <View style={Styles.menuBtn}>
                                        <View style={Styles.menuBtnInner}>
                                            <Image style={Styles.iconBtn} source={require('./img/icons/vazao_ponta.png')} />
                                        </View>
                                        <Text style={Styles.textBtn}>Vazão de Ponta</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={Grid.row}>
                            <View style={Grid.col}>
                                <TouchableOpacity ref="touch" style={Styles.touchMenuBtn} onPress={() => navigation.navigate('Calibracao')}>
                                    <View style={Styles.menuBtn}>
                                        <View style={Styles.menuBtnInner}>
                                            <Image style={Styles.iconBtn} source={require('./img/icons/calibracao.png')} />
                                        </View>
                                        <Text style={Styles.textBtn}>Calibração</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={Grid.col}>
                                <TouchableOpacity ref="touch" style={Styles.touchMenuBtn} onPress={() => navigation.navigate('SelecaoPontas')}>
                                    <View style={Styles.menuBtn}>
                                        <View style={Styles.menuBtnInner}>
                                            <Image style={Styles.iconBtn} source={require('./img/icons/selecao_pontas.png')} />
                                        </View>
                                        <Text style={Styles.textBtn}>Sel. de Ponta</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={Grid.col}>
                                <TouchableOpacity ref="touch" style={Styles.touchMenuBtn} onPress={() => this.logout()}>
                                    <View style={Styles.menuBtn}>
                                        <View style={Styles.menuBtnInner}>
                                            <Icon name="power-off" size={32} color={'#FFFFFF'} />
                                        </View>
                                        <Text style={Styles.textBtn}>Sair</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground >
        )
    }
}

MainMenu.navigationOptions = {
    title: 'Herbicat',
}

const mapStateToProps = state => {
    return {
        state: state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        add: (name) => {
            dispatch(addUserName(name))
        },
        get: () => {
            return dispatch(getUserName())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu)
