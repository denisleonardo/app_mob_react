
//action.js
import { ADD_VARIAVEIS } from './types';

export const addVariaveis = (volume,vazao) => {
    return {
        type: ADD_VARIAVEIS,
        volume:volume,
        vazao:vazao
    }
}