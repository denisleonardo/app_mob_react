
//action.js
import { ADD_PONTAS } from './types';

export const addPontas = (modelo, tamanho, angulo) => {
    return {
        type: ADD_PONTAS,
        modelo: modelo,
        tamanho: tamanho,
        angulo: angulo
    }
}