
//action.js
import { ADD_VELOCIDADE } from './types';

export const addVelocidade = value => {
    return {
        type: ADD_VELOCIDADE,
        payload: value
    }
}