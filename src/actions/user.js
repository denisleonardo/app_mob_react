
//action.js
import { ADD_USER_NAME } from './types';

export const addUserName = name => {
    return {
        type: ADD_USER_NAME,
        payload: name
    }
}