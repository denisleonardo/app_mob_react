import { Platform } from 'react-native';
import firebase from '@react-native-firebase/app';
// pluck values from your `GoogleService-Info.plist` you created on the firebase console
const iosConfig = {
    clientId: 'x',
    appId: 'x',
    apiKey: 'x',
    databaseURL: 'x',
    storageBucket: 'x',
    messagingSenderId: 'x',
    projectId: 'x',

    // enable persistence by adding the below flag
    persistence: true,
};

// pluck values from your `google-services.json` file you created on the firebase console
const androidConfig = {
    clientId: '778396433629-vi7b4bi1qdif6l7b9krbvqcc3ocfp9gj.apps.googleusercontent.com',
    appId: '1:778396433629:android:064f62e9359e30f72c2f34',
    apiKey: 'AIzaSyCf05ZhBVR4wwZIMTWuuGzf49aapDpNIh8',
    databaseURL: 'https://herbicat-3db13.firebaseio.com',
    storageBucket: 'herbicat-3db13.appspot.com',
    messagingSenderId: '',
    projectId: 'herbicat-3db13',

    // enable persistence by adding the below flag
    persistence: true,
};

const herbicat = !firebase.apps.length ? firebase.initializeApp(
    // use platform-specific firebase config
    Platform.OS === 'ios' ? iosConfig : androidConfig,
) : firebase.app();
