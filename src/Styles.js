import React from 'react';
import { StyleSheet } from 'react-native';
export const UserLogin = StyleSheet.create({
    fieldset: {
        backgroundColor: '#FFFFFF',
        borderRadius: 50,
        borderColor: '#DDDDDD',
        borderWidth: 1,
        fontSize: 14,
        textAlign: 'center',
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 15,
        paddingRight: 10,
        width: '80%',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    icon: {
        padding: 10,
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: '#424242',
    },
});
export const Styles = StyleSheet.create({
    hr: {
        flex: 1,
        borderBottomColor: '#DDDDDD',
        borderBottomWidth: 1,
        height: 5,
        width: '60%',
        alignSelf: 'center',
        marginTop: 15,
        marginBottom: 15
    },
    logoScreenLogin: {
        width: 160,
        height: 51,
        marginTop: 50,
        marginBottom: 30,
        alignSelf: 'center'
    },
    h4: {
        color: '#FFFFFF',
        fontSize: 12
    },
    header: {
        backgroundColor: '#005f27',
    },
    headerBody: {
        backgroundColor: '#005f27',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
    },
    logoImage: {
        width: 150,
        height: 48,
        marginTop: 10,
        marginBottom: 10
    },
    textBtn: {
        color: '#000000',
        fontSize: 12
    },
    touchMenuBtn: {
        color: '#000000',
        margin: 10,
    },
    menuBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        textAlign: 'center',
        flexDirection: 'column',
        width: 90,
        height: 80,
        color: '#000000'
    },
    menuBtnInner: {
        backgroundColor: '#005f27',
        paddingTop: 12,
        paddingBottom: 12,
        paddingLeft: 12,
        paddingRight: 12,
        marginBottom: 5,
        borderRadius: 100
    },
    iconBtn: {
        width: 32,
        height: 32,
    },
    result: {
        borderRadius: 3,
        width: '70%',
        fontSize: 20,
        backgroundColor: '#afca08',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        flexDirection: 'column',
        textAlignVertical: 'center',
        textAlign: 'center',
        padding: 15
    }
});
//Grid criada
export const Grid = StyleSheet.create({
    wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
    },
    wrapperCentered: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row',
        marginBottom: 10,
        width: '90%',
    },
    row2: {
        flexDirection: 'row',
        marginBottom: 10,
        width: '90%',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
    },
    col: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        textAlign: 'center',
        flexDirection: 'column',
        width: '80%'
    },
    col2: {
        padding: 5
    },
    colRow: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        textAlign: 'center',
        flexDirection: 'row',
        width: '80%'
    }
});

export const Form = StyleSheet.create({
    label: {
        color: '#39563B'
    },
    selectDefault: {
        borderRadius: 3,
        marginTop: 5,
        marginBottom: 10,
        height: 32,
        fontSize: 5,
        borderWidth: 0,
        backgroundColor: '#FFFFFF',
        width: '90%'
    },
    selectDefaultItem: {
        fontSize: 5,
        textAlign: 'center'
    },
    default: {
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        borderColor: '#FFFFFF',
        borderWidth: 1,
        fontSize: 14,
        textAlign: 'center',
        padding: 0,
        width: '90%'
    },
    defaultPonta: {
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        borderColor: '#FFFFFF',
        borderWidth: 1,
        fontSize: 14,
        textAlign: 'center',
        margin: 5,
        width: 80
    },
    defaultSM: {
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        borderColor: '#FFFFFF',
        borderWidth: 1,
        fontSize: 20,
        textAlign: 'center',
        padding: 0,
        width: '50%',
        padding: 5
    },
    textResult: {
        borderRadius: 3,
        width: '90%',
        fontSize: 14,
        borderWidth: 1,
        borderColor: '#afca08',
        backgroundColor: '#afca08',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        flexDirection: 'column',
        textAlignVertical: 'center',
        textAlign: 'center',
        height: 30
    },
    textResultSM: {
        borderRadius: 3,
        width: '25%',
        fontSize: 14,
        borderWidth: 1,
        borderColor: '#afca08',
        backgroundColor: '#afca08',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        flexDirection: 'column',
        textAlignVertical: 'center',
        textAlign: 'center',
        height: 30
    },
    button: {
        borderRadius: 50,
        borderColor: 'transparent',
        backgroundColor: '#005f27',
        marginTop: 15,
        marginBottom: 12,
        height: 40,
        width: 150,
        justifyContent: 'center',
        alignSelf: 'center',
        textAlign: 'center'
    },
    buttonLink: {
        borderWidth: 0,
        borderRadius: 50,
        borderColor: 'transparent',
        backgroundColor: 'transparent',
        marginTop: 0,
        marginBottom: 0,
        height: 40,
        width: 150,
        justifyContent: 'center',
        alignSelf: 'center',
        textAlign: 'center'
    },
    buttonXS: {
        borderRadius: 500,
        borderColor: 'transparent',
        backgroundColor: '#005f27',
        justifyContent: 'center',
        alignSelf: 'center',
        padding: 18,
        marginLeft: 4,
        marginRight: 4
    },
    buttonText: {
        color: '#FFFFFF',
        textAlign: 'center'
    },
    buttonTextLink: {
        color: '#333333',
        textAlign: 'center'
    }
});

export const Cron = StyleSheet.create({
    container: {
        borderRadius: 2,
        backgroundColor: '#FFFFFF',
        padding: 3,
        width: '60%',
        textAlign: 'center'
    },
    text: {
        fontSize: 30,
        color: '#333333',
        marginLeft: 7,
        textAlign: 'center'
    }
});