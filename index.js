/**
 * @format
 */
import React from 'react'
import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import Routes from "./src";

//Provider
import { Provider } from 'react-redux';

//store
import configureStore from './src/store';

const store = configureStore();

const RNRedux = () => {
    return (
        <Provider store={store}>
            <Routes />
        </Provider>
    )
}

AppRegistry.registerComponent(appName, () => RNRedux);
